import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://lesson67-d8927.firebaseio.com/'
});

export default instance;