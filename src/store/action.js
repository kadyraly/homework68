import axios from '../axios';

export const ADD = 'ADD';
export const HANDLECHANGE = 'HANDLECHANGE';
export const REMOVE = 'REMOVE';
export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_ERROR = 'FETCH_COUNTER_ERROR';

export const fetchCounterRequest = () => {
    return {type: FETCH_COUNTER_REQUEST};
};

export const fetchCounterSuccess = (currentTask) => {
    return {type: FETCH_COUNTER_SUCCESS, currentTask: currentTask || {}};
};

export const fetchCounterError = () => {
    return {type: FETCH_COUNTER_ERROR};
};

export const fetchTask =()=> {
    return(dispatch, getState) => {
        dispatch(fetchCounterRequest());
        axios.get('/todo.json').then(response => {
            dispatch(fetchCounterSuccess(response.data));
        }, error => {
            dispatch(fetchCounterError())
        });
    }

};
export const addTask = (todo) => {
    return(dispatch, getState) => {
        dispatch(fetchCounterRequest());
        axios.post('/todo.json', {todo: todo}).then(response => {
            dispatch(fetchTask());
        }, error=> {
            dispatch(fetchCounterError())
        });
    }
};

export const handleChange = (value) => {
    return {type: HANDLECHANGE, value: value};
};

export const removeTask = (taskId) => {
    return(dispatch, getState) => {
        dispatch(fetchCounterRequest());
       axios.delete(`/todo/${taskId}.json`).then(response => {

        dispatch(fetchTask());
       }, error => {
          dispatch(fetchCounterError())
       });
    }

};