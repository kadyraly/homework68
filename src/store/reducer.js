import {ADD, FETCH_COUNTER_SUCCESS, HANDLECHANGE, REMOVE} from "./action";

const initialState = {
    currentTask: '',
    tasks: {}
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD:
            return {...state, tasks: state.tasks, currentTask: state.currentTask};
        case REMOVE:
            return {...state, tasks: state.tasks, currentTask: state.currentTask};
        case HANDLECHANGE:
            return {...state, currentTask: action.value};
        case FETCH_COUNTER_SUCCESS:
            return {...state, tasks: action.currentTask};
        default:

            return state;
    }
};

export default reducer;