import React from 'react';
import './TaskItem.css';

const TaskItem = props => {
    return(

        <div className="taskWrapper">
            <p>{props.todo}</p>
            <button className="removeTask" onClick={props.remove} > remove  </button>
        </div>
    )
};
export default TaskItem;