import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://homework68-752fe.firebaseio.com/\n'
});

export default instance;