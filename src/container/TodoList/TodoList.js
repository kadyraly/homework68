import React, { Component } from 'react';
import './TodoList.css';
import {connect} from 'react-redux';
import TaskInput from "../../component/TaskInput/TaskInput";
import TaskItem from "../../component/TaskItem/TaskItem";

import {addTask, fetchTask, handleChange, removeTask} from '../../store/action';

class TodoList extends Component {


    componentDidMount() {
        this.props.fetchTask();

    };


    render() {

        let tasksId = Object.keys(this.props.tasks);
        return (
            <div className="App">
                <div className="task-wrapper">

                    <TaskInput changeCurrentTask = {this.props.handleChange}
                               currentTask={this.props.currentTask}
                               click={() => this.props.addTask(this.props.currentTask)}
                    />

                    <ul>
                        {
                            tasksId.map((taskId) => {
                                return <TaskItem todo={this.props.tasks[taskId].todo} key={taskId}
                                                 remove={() => this.props.removeTask(taskId)}/>
                            })
                        }
                    </ul>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        currentTask: state.currentTask,
        tasks: state.tasks
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchTask: () => dispatch(fetchTask()),
        addTask: (todo) => dispatch(addTask(todo)),
        handleChange: (e) => dispatch(handleChange(e.target.value)),
        removeTask: (id) => dispatch(removeTask(id))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(TodoList);